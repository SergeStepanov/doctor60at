package com.niitkd.doctorterminal.services;

import android.os.Environment;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class ReportService {

    private static String time;

    private static String name;
    private static String machineNumber;
    private static String serialNumber;

    private static final String defaultValue = "Не измерено";

    private static String compressorOn = defaultValue;
    private static String compressorOff = defaultValue;
    private static String compressorProductivity = defaultValue;

    private static String valveSensibility = defaultValue;
    private static String airWorkerBrakes = defaultValue;
    private static String airWorkerSecond = defaultValue;
    private static String sourceDensity = defaultValue;
    private static String breaksDensity = defaultValue;

    public static void setBuWorker(String buWorker) {
        ReportService.buWorker = buWorker;
    }

    public static void setBreaksFillingTime(String breaksFillingTime) {
        ReportService.breaksFillingTime = breaksFillingTime;
    }

    private static String buWorker = defaultValue;
    private static String tankStorageDensity = defaultValue;

    private static String kmTankStorageSensibility = defaultValue;
    private static String kmServiceBreaksTime = defaultValue;
    private static String kmEmergencyBreaksTime = defaultValue;
    private static String kmBrakesOverstress = defaultValue;
    private static String kmOvercharge = defaultValue;
    private static String kmAirPassage = defaultValue;

    private static String sourceSensibilitySecond = defaultValue;
    private static String sourceSensibilityThird = defaultValue;
    private static String sourceSensibilityFourth = defaultValue;
    private static String breaksFillingTime = defaultValue;

    private static String kvtReleaseTime = defaultValue;
    private static String kvtFillingTime = defaultValue;
    private static String kvtBreaksCylindersDensity = defaultValue;
    private static String kvtBreaksCylindersPressure = defaultValue;

    public  static void clearReport() {
        buWorker = defaultValue;
        tankStorageDensity = defaultValue;

        kmTankStorageSensibility = defaultValue;
        kmServiceBreaksTime = defaultValue;
        kmEmergencyBreaksTime = defaultValue;
        kmBrakesOverstress = defaultValue;
        kmOvercharge = defaultValue;
        kmAirPassage = defaultValue;

        sourceSensibilitySecond = defaultValue;
        sourceSensibilityThird = defaultValue;
        breaksFillingTime = defaultValue;

        kvtReleaseTime = defaultValue;
        kvtFillingTime = defaultValue;
        kvtBreaksCylindersDensity = defaultValue;
        kvtBreaksCylindersPressure = defaultValue;
        compressorOn = defaultValue;
        compressorOff = defaultValue;
        compressorProductivity = defaultValue;

        valveSensibility = defaultValue;
        airWorkerBrakes = defaultValue;
        airWorkerSecond = defaultValue;
        sourceDensity = defaultValue;
        breaksDensity = defaultValue;
    }

    public static void makeReport() {
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File (sdCard.getAbsolutePath() + "/ОТЧЕТЫ");

        String fileName = machineNumber+ "-" + serialNumber + "-" + name;
        /*time.replace(".", "_");
        fileName = fileName.replace(" ", "-");
        fileName = fileName.replace(":", "-");*/

        File file = new File(dir, fileName + ".txt");

        try (PrintWriter writer = new PrintWriter(file)) {
            writer.println("Время и дата испытания: " + time);
            writer.println("ФИО испытателя: " + name);
            writer.println("Номер локомотива: " + machineNumber);
            writer.println("Номер секции: " + serialNumber);
            writer.println("Давление включения компрессора: " + compressorOn);
            writer.println("Давление выключения компрессора: " + compressorOff);
            writer.println("Производительность компрессора: " + compressorProductivity);
            writer.println("Чувствительность золотника и золотникового питательного клапана: " + valveSensibility);
            writer.println("Плотность питательной магистрали: " + sourceDensity);
            writer.println("Плотность тормозной магистрали: " + breaksDensity);
            writer.println("Плотность уравнительного резервуара: " + tankStorageDensity);
            writer.println("КМ 394/Темп служебной разрядки ТМ:" + kmServiceBreaksTime);
            writer.println("KM 394/Темп экстренной разрядки ТМ: " + kmEmergencyBreaksTime);
            writer.println("КМ 394/Завышение давления в ТМ: " + kmBrakesOverstress);
            writer.println("КМ 394/Время ликвидации сверхзаряда: " + kmOvercharge);
            writer.println("Чувствительность питания при установке крана машиниста в положение II: " + sourceSensibilitySecond);
            writer.println("Чувствительность питания при установке крана машиниста в положение III:" + sourceSensibilityThird);
            writer.println("Чувствительность питания при установке крана машиниста в положение IV:" + sourceSensibilityFourth);
            writer.println("Время наполнения ТМ: " + breaksFillingTime);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        ReportService.name = name;
    }

    public static void setMachineNumber(String machineNumber) {
        ReportService.machineNumber = machineNumber;
    }

    public static void setSerialNumber(String serialNumber) {
        ReportService.serialNumber = serialNumber;
    }

    public static void setBreaksDensity(String breaksDensity) {
        ReportService.breaksDensity = breaksDensity;
    }

    public static void setSourceDensity(String sourceDensity) {
        ReportService.sourceDensity = sourceDensity;
    }

    public static void setValveSensibility(String valveSensibility) {
        ReportService.valveSensibility = valveSensibility;
    }

    public static void setTime(String time) {
        ReportService.time = time;
    }

    public static void setKvtReleaseTime(String kvtReleaseTime) {
        ReportService.kvtReleaseTime = kvtReleaseTime;
    }

    public static void setKvtFillingTime(String kvtFillingTime) {
        ReportService.kvtFillingTime = kvtFillingTime;
    }

    public static void setKvtBreaksCylindersDensity(String kvtBreaksCylindersDensity) {
        ReportService.kvtBreaksCylindersDensity = kvtBreaksCylindersDensity;
    }

    public static void setKvtBreaksCylindersPressure(String kvtBreaksCylindersPressure) {
        ReportService.kvtBreaksCylindersPressure = kvtBreaksCylindersPressure;
    }

    public static void setKmTankStorageSensibility(String kmTankStorageSensibility) {
        ReportService.kmTankStorageSensibility = kmTankStorageSensibility;
    }

    public static void setKmServiceBreaksTime(String kmServiceBreaksTime) {
        ReportService.kmServiceBreaksTime = kmServiceBreaksTime;
    }

    public static void setKmEmergencyBreaksTime(String kmEmergencyBreaksTime) {
        ReportService.kmEmergencyBreaksTime = kmEmergencyBreaksTime;
    }

    public static void setKmBrakesOverstress(String kmBrakesOverstress) {
        ReportService.kmBrakesOverstress = kmBrakesOverstress;
    }

    public static void setKmOvercharge(String kmOvercharge) {
        ReportService.kmOvercharge = kmOvercharge;
    }

    public static void setKmAirPassage(String kmAirPassage) {
        ReportService.kmAirPassage = kmAirPassage;
    }

    public static void setSourceSensibilitySecond(String sourceSensibilitySecond) {
        ReportService.sourceSensibilitySecond = sourceSensibilitySecond;
    }

    public static void setSourceSensibilityThird(String sourceSensibilityThird) {
        ReportService.sourceSensibilityThird = sourceSensibilityThird;
    }
    public static void setSourceSensibilityFourth(String sourceSensibilityThird) {
        ReportService.sourceSensibilityFourth = sourceSensibilityThird;
    }

    public static void setAirWorkerBrakes(String airWorkerBrakes) {
        ReportService.airWorkerBrakes = airWorkerBrakes;
    }

    public static void setAirWorkerSecond(String airWorkerSecond) {
        ReportService.airWorkerSecond = airWorkerSecond;
    }

    public static void setTankStorageDensity(String tankStorageDensity) {
        ReportService.tankStorageDensity = tankStorageDensity;
    }

    public static void setCompressorOn(String compressorOn) {
        ReportService.compressorOn = compressorOn;
    }

    public static void setCompressorProductivity(String compressorProductivity) {
        ReportService.compressorProductivity = compressorProductivity;
    }

    public static void setCompressorOff(String compressorOff) {
        ReportService.compressorOff = compressorOff;
    }
}
