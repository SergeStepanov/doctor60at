package com.niitkd.doctorterminal.communication;

public class Response {

    private byte address;
    private byte command;
    private String data;
    private byte[] crc;

    public Response (byte[] request) {
        address = request[0];
        command = request[1];
        data = "failure";
        crc = new byte[]{1, 1};
    }

    public Response(byte address, byte command, String data, byte[] crc) {
        this.address = address;
        this.command = command;
        this.data = data;
        this.crc = crc;
    }

    public byte getAddress() {
        return address;
    }

    public void setAddress(byte address) {
        this.address = address;
    }

    public byte getCommand() {
        return command;
    }

    public void setCommand(byte command) {
        this.command = command;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public byte[] getCrc() {
        return crc;
    }

    public void setCrc(byte[] crc) {
        this.crc = crc;
    }
}
