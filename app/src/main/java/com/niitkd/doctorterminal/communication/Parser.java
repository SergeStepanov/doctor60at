package com.niitkd.doctorterminal.communication;

import com.niitkd.doctorterminal.communication.codes.Requests;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class Parser {
    public Response parseResponse(byte[] response) {
        byte address = response[0];
        byte command = response[1];
        byte[] data = Arrays.copyOfRange(response, 2, 6);
        byte[] crc = Arrays.copyOfRange(response, 6, 8);

        byte[] successCode = {(byte) 0xEE, (byte) 0xEE, (byte) 0xEE, (byte) 0xEE};
        byte[] errorCode = {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
        byte[] waitingCode = {(byte) 0x46, (byte) 0x55, (byte) 0x43, (byte) 0x4B};
        byte[] cancelMeasurement = {(byte) 0xFE, (byte) 0xFE, (byte) 0xFE, (byte) 0xFE};
        byte diagnosticsCommand = (byte) 0xAA;

        if (Arrays.equals(data, successCode)) {
            return new Response(address, command, "success", crc);
        }
        if (Arrays.equals(data, errorCode)) {
            return new Response(address, command, "failure", crc);
        }
        if (Arrays.equals(data, waitingCode) && command == diagnosticsCommand) {
            return new Response(address, command, "waiting", crc);
        }
        if (Arrays.equals(data, cancelMeasurement)) {
            return new Response(address, command, "cancel", crc);
        }

        byte[] onData = new byte[4];
        byte[] offData = new byte[4];
        Arrays.fill(onData, (byte) 0);
        Arrays.fill(offData, (byte) 0);
        onData[2] = data[0];
        onData[3] = data[1];
        offData[2] = data[2];
        offData[3] = data[3];

        if (command == Requests.COMPRESSOR_ON_OFF_PRESSURES.getCommand()) {
            int onValue = ByteBuffer.wrap(onData).getInt();
            int offValue = ByteBuffer.wrap(offData).getInt();
            return new Response(address, command, String.valueOf((double)onValue/1000) + " " + String.valueOf((double)offValue/1000), crc);
        }
        else {
            int value = ByteBuffer.wrap(data).getInt();
            return new Response(address, command, String.valueOf((double)value/1000), crc);
        }
    }
}
