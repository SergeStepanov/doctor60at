package com.niitkd.doctorterminal.communication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Looper;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;

import com.niitkd.doctorterminal.communication.codes.Requests;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;

import static com.niitkd.doctorterminal.utils.BatteryHelper.changeBatteryView;

public class BatteryTask extends AsyncTask<byte[], Response, Response>{

    private WeakReference<Activity> activity;
    private ProgressDialog dialog;

    public BatteryTask(Activity activity) {
        this.activity = new WeakReference<>(activity);
        dialog = new ProgressDialog(activity);
        SpannableString message = new SpannableString("Опрос заряда батареи");
        message.setSpan(new RelativeSizeSpan(2f), 0, message.length(), 0);
        dialog.setMessage(message);
        dialog.setCancelable(false);
    }

    @Override
    protected Response doInBackground(byte[]... bytes) {
        while(true) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (isCancelled())
                return new Response(bytes[0]);
            try {
                Response measurementStarted = new Response(bytes[0]);
                publishProgress(measurementStarted);
                Socket socket = new Socket(InetAddress.getByName("192.168.1.55"), 88);
                DataOutputStream out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                DataInputStream is = new DataInputStream(socket.getInputStream());
                out.write(Requests.BATTERY.getBytes());
                out.flush();

                Parser parser = new Parser();

                byte[] inputBuffer = new byte[8];

                AsyncReceiver.waitForResponse(is, inputBuffer, 8000);
                measurementStarted = parser.parseResponse(inputBuffer);

                Arrays.fill(inputBuffer, (byte) 0);
                socket.close();
                socket = new Socket(InetAddress.getByName("192.168.1.55"), 88);
                out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                is = new DataInputStream(socket.getInputStream());
                if (measurementStarted.getData().equals("success")) {
                    AsyncReceiver.waitForResponse(is, inputBuffer, 10000);
                    Response batteryCharge = parser.parseResponse(inputBuffer);
                    publishProgress(batteryCharge);
                    Arrays.fill(inputBuffer, (byte) 0);
                }
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                dialog.dismiss();
            }
            try {
                Thread.sleep(
                        600000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onProgressUpdate(Response... values) {
        super.onProgressUpdate(values);
        final String data = values[0].getData();
        byte command = values[0].getCommand();
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        else {
            dialog.show();
        }
        if (data.equals("success") || data.equals("failure"))
            return;

        if (command == Requests.BATTERY.getCommand()) {
            new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                   changeBatteryView(data);
                }
            });
        }
    }
}
