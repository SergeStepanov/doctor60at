package com.niitkd.doctorterminal.communication.codes;

public enum Requests {

    YR_DENSITY                  ((byte) 0x10, (byte) 0x1F, (byte) 0xCC, (byte) 0xCC),
    BRAKES_DENSITY              ((byte) 0x10, (byte) 0x2F, (byte) 0xCC, (byte) 0xCC),
    SOURCE_DENSITY_STATE        ((byte) 0x10, (byte) 0x3F, (byte) 0xCC, (byte) 0xCC),
    SOURCE_DENSITY              ((byte) 0x10, (byte) 0x3E, (byte) 0xCC, (byte) 0xCC),
    VALVE_SENSIBILITY           ((byte) 0x10, (byte) 0x4F, (byte) 0xCC, (byte) 0xCC),
    WORK_AIRDISTRIBUTOR         ((byte) 0x10, (byte) 0x5F, (byte) 0xCC, (byte) 0xCC),
    SENSIBILITY_POWER_TWO       ((byte) 0x10, (byte) 0xDF, (byte) 0xCC, (byte) 0xCC),
    SENSIBILITY_POWER_THREE     ((byte) 0x10, (byte) 0xEF, (byte) 0xCC, (byte) 0xCC),
    SENSIBILITY_POWER_FOUR      ((byte) 0x10, (byte) 0xDF, (byte) 0xCC, (byte) 0xCC),
    TIME_FILLING_BRAKE          ((byte) 0x10, (byte) 0xCC, (byte) 0xCC, (byte) 0xCC),
    WORK_BY_FIRST               ((byte) 0x10, (byte) 0xB7, (byte) 0xCC, (byte) 0xCC),
    DENSITY_BRAKE_CYLINDER      ((byte) 0x10, (byte) 0xBA, (byte) 0xCC, (byte) 0xCC),
    TIME_RELEASE                ((byte) 0x10, (byte) 0xBB, (byte) 0xCC, (byte) 0xCC),
    TIME_FILLING                ((byte) 0x10, (byte) 0xBC, (byte) 0xCC, (byte) 0xCC),
    AIR_PASSAGE                 ((byte) 0x10, (byte) 0xCF, (byte) 0xCC, (byte) 0xCC),
    TIME_OVERCHARGE_FIRST       ((byte) 0x10, (byte) 0xBF, (byte) 0xCC, (byte) 0xCC),
    TIME_OVERCHARGE_SECOND      ((byte) 0x10, (byte) 0xB2, (byte) 0xCC, (byte) 0xCC),
    OVERSTRESS_BRAKE            ((byte) 0x10, (byte) 0xB6, (byte) 0xCC, (byte) 0xCC),
    EMERGENCY_BRAKING           ((byte) 0x10, (byte) 0x9F, (byte) 0xCC, (byte) 0xCC),
    SENSIBILITY_STORAGE_TANK    ((byte) 0x10, (byte) 0x7F, (byte) 0xCC, (byte) 0xCC),
    SERVICE_BRAKING             ((byte) 0x10, (byte) 0x8F, (byte) 0xCC, (byte) 0xCC),
    WORK_AIR_DISTRIBUTOR_TWO    ((byte) 0x10, (byte) 0xB8, (byte) 0xCC, (byte) 0xCC),
    PRESSURE_BRAKE_CYLINDER     ((byte) 0x10, (byte) 0xBE, (byte) 0xCC, (byte) 0xCC),
    COMPRESSOR_ON_OFF_PRESSURES ((byte) 0x10, (byte) 0x99, (byte) 0xCC, (byte) 0xCC),
    COMPRESSOR_CHARGE           ((byte) 0x10, (byte) 0x88, (byte) 0xCC, (byte) 0xCC),
    COMPRESSOR_EFFICIENCY       ((byte) 0x10, (byte) 0x89, (byte) 0xCC, (byte) 0xCC),

    TEST_VALVES                 ((byte) 0x10, (byte) 0xAB, (byte) 0xCC, (byte) 0xCC),
    CHECK_CONNECTION_STATUS     ((byte) 0x10, (byte) 0xAA),
    BATTERY                     ((byte) 0x10, (byte) 0xAF, (byte) 0xCC, (byte) 0xCC),
    RESTART_WIFI_MODULE         ((byte) 0x10, (byte) 0xAC, (byte) 0xCC, (byte) 0xCC);

    private byte[] bytes;

    Requests(byte... bytes) {
        this.bytes = bytes;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public byte getCommand() {
        return this.bytes[1];
    }
}
