package com.niitkd.doctorterminal.communication;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.ui.OnDataSentToActivity;
import com.niitkd.doctorterminal.ui.activities.km394.TankStorageSensibility;
import com.niitkd.doctorterminal.ui.activities.other.BUActivity;
import com.niitkd.doctorterminal.ui.activities.other.BrakesFillingTimeActivity;
import com.niitkd.doctorterminal.ui.activities.other.AirDistributorBrakesActivity;
import com.niitkd.doctorterminal.ui.activities.other.WorkAirDistributorTwoActivity;
import com.niitkd.doctorterminal.ui.activities.km394.EmergencyBrakingActivity;
import com.niitkd.doctorterminal.ui.activities.km394.ServiceBrakingActivity;
import com.niitkd.doctorterminal.ui.activities.kvt.KvtFillingTimeActivity;
import com.niitkd.doctorterminal.ui.activities.kvt.KvtReleaseTimeActivity;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;

public class AsyncReceiver extends AsyncTask<byte[], Integer, Response> {

    private WeakReference<OnDataSentToActivity> dataSentToActivity;
    private ProgressDialog dialog;
    private int communicationTimeout;
    private int measurementTimeout;

    //Если что-то случилось в асинхронной операции
    private Exception exception;

    public AsyncReceiver(OnDataSentToActivity activity) throws IOException {
        this.dataSentToActivity = new WeakReference<>(activity);
        dialog = new ProgressDialog((Context) activity);
        communicationTimeout = 7000;
        measurementTimeout = 7000;
    }

    public static void waitForResponse (DataInputStream is, byte[] buffer, int timeout) throws IOException {
        long maxTimeMillis = System.currentTimeMillis() + timeout;
        int bufferOffset = 0;

        while(System.currentTimeMillis() < maxTimeMillis && bufferOffset < buffer.length) {
            int readLength = Math.min(is.available(), buffer.length - bufferOffset);
            int readResult = is.read(buffer, bufferOffset, readLength);
            if (readResult == -1) break;
            bufferOffset += readResult;
        }
    }

    @Override
    protected void onPreExecute() {
        SpannableString message = new SpannableString("Идет измерение");
        if (dataSentToActivity.get() instanceof EmergencyBrakingActivity) {
            message = new SpannableString("Произведите экстренное торможение");
        }
        if (dataSentToActivity.get() instanceof ServiceBrakingActivity) {
            message = new SpannableString("Произведите полное служебное торможение до 3.5 кгс/см" + "\u00B2");
        }
        if (dataSentToActivity.get() instanceof BrakesFillingTimeActivity) {
            message = new SpannableString("Зарядите ТМ");
        }
        if (dataSentToActivity.get() instanceof KvtFillingTimeActivity) {
            message = new SpannableString("Переведите ручку крана вспомогательного тормоза в крайнее тормозное положение");
        }
        if (dataSentToActivity.get() instanceof KvtReleaseTimeActivity) {
            message = new SpannableString("Переведите ручку крана вспомогательного тормоза в положение отпуска");
        }
        if (dataSentToActivity.get() instanceof WorkAirDistributorTwoActivity) {
            message = new SpannableString("Установите ручку крана машиниста в положение II");
        }
        if (dataSentToActivity.get() instanceof AirDistributorBrakesActivity) {
            message = new SpannableString("Cнизьте давление в ТМ на 0.5-0.6 кгс/см");
        }
        if (dataSentToActivity.get() instanceof BUActivity) {
            message = new SpannableString("Установите ручку крана машиниста в положение I");
        }
        if (dataSentToActivity.get() instanceof TankStorageSensibility) {
            message = new SpannableString("Снизьте давление в уравнительном резервуре на 0.15-0.2 кгс/cм\u00B2");
        }

        message.setSpan(new RelativeSizeSpan(2f), 0, message.length(), 0);

        dialog.setMessage(message);
        dialog.setCancelable(false);

        dialog.show();
    }

    @Override
    protected Response doInBackground (byte[]... requests) {
        Response measurementResult = new Response(requests[0]);
        Response measurementStarted;

        try {
            Socket socket = new Socket(InetAddress.getByName("192.168.1.55"), 88);
            DataOutputStream out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            DataInputStream is = new DataInputStream(socket.getInputStream());
            out.write(requests[0]);
            out.flush();

            Parser parser = new Parser();

            byte[] inputBuffer = new byte[8];

            //
            //После посылки реквеста на измерение сервер должен ответить кодом о готовности.
            //Здесь ловим этот код.
            //
            waitForResponse(is, inputBuffer, communicationTimeout);
            measurementStarted = parser.parseResponse(inputBuffer);
            Arrays.fill(inputBuffer, (byte) 0);
            Thread.sleep(500);
            socket.close();
            if (measurementStarted.getData().equals("success") &&
                    measurementStarted.getCommand() != Requests.CHECK_CONNECTION_STATUS.getCommand()) {
                long waitingTime = 0;
                socket = new Socket(InetAddress.getByName("192.168.1.55"), 88);
                out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                is = new DataInputStream(socket.getInputStream());
                //
                //Ждем результат измерения, если в процессе ожидания от сервера прилетает код ожидания,
                //то ждем еще.
                //
                while (waitingTime < measurementTimeout) {
                    waitForResponse(is, inputBuffer, measurementTimeout);
                    if (inputBuffer[0] != 0 && inputBuffer[1] != 0)
                        measurementResult = parser.parseResponse(inputBuffer);
                    Arrays.fill(inputBuffer, (byte) 0);
                    waitingTime = measurementTimeout;
                    if(measurementResult.getData().equals("waiting")) {
                        waitingTime = 0;
                        measurementResult.setData("failure");
                        Thread.sleep(500);
                        socket.close();
                        socket = new Socket(InetAddress.getByName("192.168.1.55"), 88);
                        out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                        is = new DataInputStream(socket.getInputStream());
                    }
                }
            }

            //
            //Если не удалось ничего получить от сервера, то шлем ему команду перезагрузки wi-fi
            //модуля
            //
            if (measurementResult.getData().equals("failure")) {
                out.write(Requests.RESTART_WIFI_MODULE.getBytes());
                out.flush();
                publishProgress();
                Thread.sleep(10000);
            }
            socket.close();
        } catch (IOException e) {
            exception = e;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return measurementResult;
    }

    @Override
    protected void onPostExecute(Response result) {
        super.onPostExecute(result);
        if (dialog.isShowing())
            dialog.dismiss();

        if (exception != null) {
            Toast toast = Toast.makeText((Context) dataSentToActivity.get(), "Нет соединения с сервером", Toast.LENGTH_LONG);
            ViewGroup group = (ViewGroup) toast.getView();
            TextView textView = (TextView) group.getChildAt(0);
            textView.setTextSize(36);
            toast.show();
            return;
        }
        OnDataSentToActivity activity = dataSentToActivity.get();
        activity.updateMeasurementData(result.getCommand(), result.getData());
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if (dialog.isShowing())
            dialog.dismiss();
        SpannableString message = new SpannableString("Возникли неполадки с соединением.");
        message.setSpan(new RelativeSizeSpan(2f), 0, message.length(), 0);
        dialog.setMessage(message);
        dialog.setCancelable(false);
        dialog.show();
    }

    public void setMeasurementTimeout(int measurementTimeout) {
        this.measurementTimeout = measurementTimeout;
    }

    public void setCommunicationTimeout(int communicationTimeout) {
        this.communicationTimeout = communicationTimeout;
    }

    public int getCommunicationTimeout() {
        return communicationTimeout;
    }

    public int getMeasurementTimeout() {
        return measurementTimeout;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
