package com.niitkd.doctorterminal.ui.activities.other;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.IOException;

public class BrakesDensityActivity extends MeasurementActivity {
    private AlertDialog measurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMeasurementTextViews(
                getResources().getString(R.string.breaksBusDensity),
                getResources().getString(R.string.sensibility_default),
                "Снижение давления в тормозной магистрали с зарядного\nза 60 с\n(норма:не более 0.2 кгс/см\u00B2)",
                "");
    }

    @Override
    public void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(BrakesDensityActivity.this);
        builder.setTitle("Проверка плотности тормозной магистрали")
                .setMessage("Установите ручку крана машиниста в поездное положение. Дождитесь зарядки ТМ. Перекройте комбинированный кран. Выключите компрессор.")
                .setCancelable(true)
                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        measurement.hide();
                        try {
                            AsyncReceiver receiver = new AsyncReceiver(BrakesDensityActivity.this);
                            receiver.setMeasurementTimeout(80000);
                            receiver.execute(Requests.BRAKES_DENSITY.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
        measurement = builder.create();
        measurement.show();
        setDialogAppearance(measurement);
    }

    @Override
    public void updateMeasurementData(byte command, String data) {
        if (command == Requests.CHECK_CONNECTION_STATUS.getCommand())
            return;
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }

        if (data.equals("success")) {
            return;
        }

        TextView textView = findViewById(R.id.measurementResult);
        textView.setText(data);
        textView.append(" кгс/см" + "\u00B2");
        String result = new String(data + " кгс/см" + "\u00B2");
        if (Double.parseDouble(data) <= 0.2){
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setBreaksDensity(result);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
