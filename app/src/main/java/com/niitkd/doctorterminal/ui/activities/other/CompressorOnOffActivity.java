package com.niitkd.doctorterminal.ui.activities.other;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.IOException;

public class CompressorOnOffActivity extends MeasurementActivity {

    android.support.v7.app.AlertDialog measurement;
    RadioButton radioButtonBig;
    RadioButton radioButtonSmall;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMeasurementTextViews(
                getResources().getString(R.string.compressorOnOff),
                "Включение: 0 кгс/см\u00B2" + "\nВыключение: 0 кгс/см\u00B2",
                "Давления в главных резервуарах, при которых включается и отключается компрессор",
                ""
        );

        text = findViewById(R.id.textViewСompressor);
        text.setVisibility(View.VISIBLE);
        radioButtonBig = findViewById(R.id.radioButtonBig);
        radioButtonSmall = findViewById(R.id.radioButtonSmall);
        radioButtonBig.setVisibility(View.VISIBLE);
        radioButtonSmall.setVisibility(View.VISIBLE);
        TextView result = findViewById(R.id.measurementResult);
        result.setTextSize(48);
    }

    @Override
    public void updateMeasurementData(byte command, String data) {
        if (command == Requests.CHECK_CONNECTION_STATUS.getCommand())
            return;
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }

        if (data.equals("success")) {
            return;
        }

        String on = data.split(" ")[0];
        String off;

        if (radioButtonSmall.isChecked()) {
            off = String.valueOf(Double.parseDouble(data.split(" ")[1]) - 0.45); // magic
        } else {
            off = data.split(" ")[1];
        }

        measurementResult.setText("Включение: " + on + " кгс/см\u00B2\n" +
                                  "Выключение: " + off + " кгс/см\u00B2");

        String resultOn = new String(on + " кгс/см" + "\u00B2");
        String resultOff = new String(off + " кгс/см" + "\u00B2");
        if (Double.parseDouble(on) > 7.2 && Double.parseDouble(on) < 7.8) {
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            resultOn = resultOn.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            resultOn = resultOn.concat(". Вне нормы");
        }
        ReportService.setCompressorOn(resultOn + " кгс/см" + "\u00B2");

        if (radioButtonSmall.isChecked()) {
            if (Double.parseDouble(off) > 8.3 && Double.parseDouble(off) < 8.7) {
                measurementIsNorm.setText("Значение входит в нормированный диапазон");
                resultOff = resultOff.concat(". Норма");
            } else {
                measurementIsNorm.setText("Значение не входит в нормированный диапазон");
                resultOff = resultOff.concat(". Вне нормы");
            }
        } else {
            if (Double.parseDouble(off) > 8.7 && Double.parseDouble(off) < 9.3) {
                measurementIsNorm.setText("Значение входит в нормированный диапазон");
                resultOff = resultOff.concat(". Норма");
            } else {
                measurementIsNorm.setText("Значение не входит в нормированный диапазон");
                resultOff = resultOff.concat(". Вне нормы");
            }
        }
        ReportService.setCompressorOff(resultOff);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.startButton) {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(CompressorOnOffActivity.this);
            builder.setTitle(getResources().getString(R.string.compressorOnOff))
                    .setMessage("Включите компрессор и зарядите главный резервуар.")
                    .setCancelable(true)
                    .setNegativeButton("Отмена",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                    .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            measurement.hide();
                            try {
                                AsyncReceiver receiver = new AsyncReceiver(CompressorOnOffActivity.this);
                                receiver.setMeasurementTimeout(90000);
                                receiver.execute(Requests.COMPRESSOR_ON_OFF_PRESSURES.getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            measurement = builder.create();
            measurement.show();
            setDialogAppearance(measurement);
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        text.setVisibility(View.INVISIBLE);
        radioButtonBig.setVisibility(View.INVISIBLE);
        radioButtonSmall.setVisibility(View.INVISIBLE);
    }
}
