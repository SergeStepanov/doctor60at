package com.niitkd.doctorterminal.ui.activities.km394;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.Parser;
import com.niitkd.doctorterminal.communication.Response;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.OnDataSentToActivity;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;

public class DensityEqualizationTankActivity extends MeasurementActivity {

    private AlertDialog firstMeasurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMeasurementTextViews(
                getResources().getString(R.string.densityYR),
                getResources().getString(R.string.sensibility_default),
                "Снижение давления в уравнительном резервуаре в течение 3 мин. при установке ручки крана машиниста в положение IV\n(норма: не более 0.1 кгс/см\u00B2)",
                "");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(DensityEqualizationTankActivity.this);
        builder.setTitle("Проверка плотности уравнительного резервуара")
                .setMessage("Зарядите УР и ТМ. Установите ручку крана машиниста в положение IV")
                .setCancelable(true)
                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            firstMeasurement.hide();
                            //AsyncReceiver receiver = new AsyncReceiver(DensityEqualizationTankActivity.this);
                            DensityEqualizationTankActivityReceiver receiver =
                                    new DensityEqualizationTankActivityReceiver(DensityEqualizationTankActivity.this);
                            receiver.setMeasurementTimeout(35000);
                            receiver.execute(Requests.YR_DENSITY.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
        firstMeasurement = builder.create();
        firstMeasurement.show();
        setDialogAppearance(firstMeasurement);
    }

    @Override
    public void updateMeasurementData(byte command, String data) {
        if (command == Requests.CHECK_CONNECTION_STATUS.getCommand())
            return;
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }
        if (data.equals("success")) {
            return;
        }

        TextView textView = findViewById(R.id.measurementResult);
        textView.setText(data);
        textView.append(" кгс/см" + "\u00B2");


        String result = new String(data + " кгс/см" + "\u00B2");
        if (Double.parseDouble(data) <= 0.1) {
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setTankStorageDensity(result);
    }

    private static class DensityEqualizationTankActivityReceiver extends AsyncReceiver {
        public DensityEqualizationTankActivityReceiver(OnDataSentToActivity activity) throws IOException {
            super(activity);
        }

        @Override
        protected Response doInBackground(byte[]... requests) {
            Response measurementResult = new Response(requests[0]);
            Response measurementStarted;
            Socket socket = new Socket();

            try {
                socket = new Socket(InetAddress.getByName("192.168.1.55"), 88);
                DataOutputStream out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                DataInputStream is = new DataInputStream(socket.getInputStream());

                out.write(requests[0]);
                out.flush();

                Parser parser = new Parser();

                byte[] inputBuffer = new byte[8];

                //
                //После посылки реквеста на измерение сервер должен ответить кодом о готовности.
                //Здесь ловим этот код.
                //
                waitForResponse(is, inputBuffer, getCommunicationTimeout());
                measurementStarted = parser.parseResponse(inputBuffer);
                Arrays.fill(inputBuffer, (byte) 0);
                socket.close();
                if (measurementStarted.getData().equals("success") &&
                        measurementStarted.getCommand() != Requests.CHECK_CONNECTION_STATUS.getCommand()) {
                    Thread.sleep(160000);
                    socket = new Socket(InetAddress.getByName("192.168.1.55"), 88);
                    out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                    is = new DataInputStream(socket.getInputStream());

                    long waitingTime = 0;

                    while (waitingTime < getMeasurementTimeout()) {
                        waitForResponse(is, inputBuffer, getMeasurementTimeout());
                        if (inputBuffer[0] != 0 && inputBuffer[1] != 0)
                            measurementResult = parser.parseResponse(inputBuffer);
                        Arrays.fill(inputBuffer, (byte) 0);
                        waitingTime = getMeasurementTimeout();
                        if(measurementResult.getData().equals("waiting")) {
                            waitingTime = 0;
                            measurementResult.setData("failure");
                            Thread.sleep(500);
                            socket.close();
                            socket = new Socket(InetAddress.getByName("192.168.1.55"), 88);
                            out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                            is = new DataInputStream(socket.getInputStream());
                        }
                    }
                }

                if (measurementResult.getData().equals("failure")) {
                    out.write(Requests.RESTART_WIFI_MODULE.getBytes());
                    out.flush();
                    publishProgress();
                    Thread.sleep(20000);
                }
            } catch (IOException e) {
                setException(e);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return measurementResult;
        }
    }
}
