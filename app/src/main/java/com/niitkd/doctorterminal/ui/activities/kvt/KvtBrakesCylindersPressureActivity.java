package com.niitkd.doctorterminal.ui.activities.kvt;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.IOException;

public class KvtBrakesCylindersPressureActivity extends MeasurementActivity {

    AlertDialog measurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMeasurementTextViews(
                getResources().getString(R.string.kvtPressureBrakeCylinder),
                getResources().getString(R.string.sensibility_default),
                "Давление в тормозных цилиндрах в зависимости от положения ручки крана вспомогательного тормоза",
                "");
    }

    @Override
    public void updateMeasurementData(byte command, String data) {
        if (command == Requests.CHECK_CONNECTION_STATUS.getCommand())
            return;
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }

        if (data.equals("success")) {
            return;
        }

        measurementResult.setText(data);
        measurementResult.append(" кгс/см" + "\u00B2");


        String result = new String(data + " кгс/см" + "\u00B2");
        if(Double.parseDouble(data) >= 1 && Double.parseDouble(data) <= 1.3 ||
                Double.parseDouble(data) >= 3.8 && Double.parseDouble(data) <= 4){
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setKvtBreaksCylindersPressure(result);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.startButton) {
            AlertDialog.Builder builder = new AlertDialog.Builder(KvtBrakesCylindersPressureActivity.this);
            builder.setTitle("Измерение давления в тормозных цилиндрах")
                    .setMessage("Установите ручку крана вспомогательного тормоза в положение I или в положение IV")
                    .setCancelable(true)
                    .setNegativeButton("Отмена",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                    .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            measurement.hide();
                            try {
                                AsyncReceiver receiver = new AsyncReceiver(KvtBrakesCylindersPressureActivity.this);
                                receiver.setMeasurementTimeout(60000);
                                receiver.execute(Requests.PRESSURE_BRAKE_CYLINDER.getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            measurement = builder.create();
            measurement.show();
            setDialogAppearance(measurement);
        }
    }
}
