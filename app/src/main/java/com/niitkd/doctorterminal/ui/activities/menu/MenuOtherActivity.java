package com.niitkd.doctorterminal.ui.activities.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.ui.activities.km394.AirPassageActivity;
import com.niitkd.doctorterminal.ui.activities.km394.BrakesOverstressActivity;
import com.niitkd.doctorterminal.ui.activities.km394.EmergencyBrakingActivity;
import com.niitkd.doctorterminal.ui.activities.km394.OverchargeActivity;
import com.niitkd.doctorterminal.ui.activities.km394.ServiceBrakingActivity;
import com.niitkd.doctorterminal.ui.activities.km394.TankStorageSensibility;
import com.niitkd.doctorterminal.ui.activities.km394.DensityEqualizationTankActivity;
import com.niitkd.doctorterminal.ui.activities.km394.SourceSensibilitySecondActivity;
import com.niitkd.doctorterminal.ui.activities.km394.SourceSensibilityThirdActivity;
import com.niitkd.doctorterminal.ui.activities.km394.ValveSensibilityActivity;
import com.niitkd.doctorterminal.ui.activities.other.AirDistributorBrakesActivity;
import com.niitkd.doctorterminal.ui.activities.other.BUActivity;
import com.niitkd.doctorterminal.ui.activities.other.BrakesDensityActivity;
import com.niitkd.doctorterminal.ui.activities.other.BrakesFillingTimeActivity;
import com.niitkd.doctorterminal.ui.activities.other.CompressorEfficiencyActivity;
import com.niitkd.doctorterminal.ui.activities.other.CompressorOnOffActivity;
import com.niitkd.doctorterminal.ui.activities.other.SourceDensityActivity;
import com.niitkd.doctorterminal.ui.activities.other.WorkAirDistributorTwoActivity;

public class MenuOtherActivity extends AppCompatActivity implements View.OnClickListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_other);

        /*Button airDistribButton = findViewById(R.id.workAirDistributor);
        Button airDistribTwoButton = findViewById(R.id.workAirDistributorTwo);
        Button workBYButton = findViewById(R.id.workBY);*/
        Button timeFillingBrakeButton = findViewById(R.id.timeFillingBrakeBus);
        Button compressorEfficiencyButton = findViewById(R.id.compressorEfficiencyButton);
        Button compressorOnOffButton = findViewById(R.id.compressorOnOffButton);
        Button sourceDensityButton = findViewById(R.id.sourceDensityButton);
        Button brakesDensityButton = findViewById(R.id.breaksDensityButton);
        Button valveSensibilityButton = findViewById(R.id.valveSensibilityButton);

       /* airDistribButton.setOnClickListener(this);
        airDistribTwoButton.setOnClickListener(this);
        workBYButton.setOnClickListener(this);*/
        timeFillingBrakeButton.setOnClickListener(this);
        compressorEfficiencyButton.setOnClickListener(this);
        compressorOnOffButton.setOnClickListener(this);
        sourceDensityButton.setOnClickListener(this);
        brakesDensityButton.setOnClickListener(this);
        valveSensibilityButton.setOnClickListener(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void onClick(View view) {
        switch(view.getId()) {
            /*case R.id.workAirDistributor:
                startActivity(new Intent(this, AirDistributorBrakesActivity.class));
                break;
            case R.id.workAirDistributorTwo:
                startActivity(new Intent(this, WorkAirDistributorTwoActivity.class));
                break;
            case R.id.workBY:
                startActivity(new Intent(this, BUActivity.class));
                break;*/
            case R.id.timeFillingBrakeBus:
                startActivity(new Intent(this, BrakesFillingTimeActivity.class));
                break;
            case R.id.compressorEfficiencyButton:
                startActivity(new Intent(this, CompressorEfficiencyActivity.class));
                break;
            case R.id.compressorOnOffButton:
                startActivity(new Intent(this, CompressorOnOffActivity.class));
                break;
            case R.id.sourceDensityButton:
                startActivity(new Intent(this, SourceDensityActivity.class));
                break;
            case R.id.breaksDensityButton:
                startActivity(new Intent(this, BrakesDensityActivity.class));
                break;
            case R.id.valveSensibilityButton:
                startActivity(new Intent(this, ValveSensibilityActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
