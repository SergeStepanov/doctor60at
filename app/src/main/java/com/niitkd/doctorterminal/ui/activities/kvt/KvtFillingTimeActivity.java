package com.niitkd.doctorterminal.ui.activities.kvt;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.IOException;

public class KvtFillingTimeActivity extends MeasurementActivity {

    AlertDialog measurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setMeasurementTextViews(
                getResources().getString(R.string.kvtFillingTime),
                getResources().getString(R.string.density_default),
                "Время, за которое давление в тормозных цилиндрах увеличится с 0 до 3,5 кгс/см2",
                "");
    }

    @Override
    public void updateMeasurementData(byte command, String data) {
        if (command == Requests.CHECK_CONNECTION_STATUS.getCommand())
            return;
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }

        if (data.equals("success")) {
            return;
        }


        TextView textView = findViewById(R.id.measurementResult);
        textView.setText(data);
        textView.append(" c");


        String result = new String(data + " c");
        TextView isNorm = findViewById(R.id.isNorm);
        if (Double.valueOf(data) > 4){
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setKvtFillingTime(result);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.startButton) {
            AlertDialog.Builder builder = new AlertDialog.Builder(KvtFillingTimeActivity.this);
            builder.setTitle("Измерение времени отпуска")
                    .setMessage("Зарядите ТМ. Переведите ручку крана вспомогательного тормоза в поездное положение")
                    .setCancelable(true)
                    .setNegativeButton("Отмена",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                    .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            measurement.hide();
                            try {
                                AsyncReceiver receiver = new AsyncReceiver(KvtFillingTimeActivity.this);
                                receiver.setMeasurementTimeout(70000);
                                receiver.execute(Requests.TIME_FILLING.getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            measurement = builder.create();
            measurement.show();
            setDialogAppearance(measurement);
        }
    }
}
