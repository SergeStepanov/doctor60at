package com.niitkd.doctorterminal.ui.activities.menu;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.services.ReportService;

public class LoginActivity extends AppCompatActivity{

    private EditText machine;
    private EditText serialNumber;
    private EditText name;
    private Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        machine = findViewById(R.id.machineEditText);
        serialNumber = findViewById(R.id.serialNumberEditText);
        name = findViewById(R.id.FIOeditText);
        nextButton = findViewById(R.id.nextButton);
    }

    public void onClick(View view) {
        if (machine.getText().toString().equals("") || name.getText().toString().equals("")
                || serialNumber.getText().toString().equals("")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Введите данные");
            builder.setMessage("Заполните все поля");
            builder.setPositiveButton("OK", null);

            AlertDialog dialog = builder.create();
            dialog.show();
            return;
/*        } else if (!name.getText().toString().matches("^[А-ЯЁ][а-яё]+[A-ЯЁ][а-яё]$")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Введите данные");
            builder.setMessage("Введено некорректное ФИО");
            builder.setPositiveButton("OK", null);

            AlertDialog dialog = builder.create();
            dialog.show();
            return;*/
        }

        Intent mainMenuActivityIntent = new Intent(this, MainMenuActivity.class);

        ReportService.setName(name.getText().toString());
        ReportService.setSerialNumber(serialNumber.getText().toString());
        ReportService.setMachineNumber(machine.getText().toString());

        startActivity(mainMenuActivityIntent);
    }
}
