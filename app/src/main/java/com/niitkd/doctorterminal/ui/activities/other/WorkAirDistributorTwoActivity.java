package com.niitkd.doctorterminal.ui.activities.other;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.IOException;

/**
 * Created by KruzeII on 23.12.2017.
 */

public class WorkAirDistributorTwoActivity extends MeasurementActivity {

    private AlertDialog measurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMeasurementTextViews(
                getResources().getString(R.string.airDistributorTwo),
                getResources().getString(R.string.sensibility_default),
                "Снижение давления в тормозных цилиндрах при установке ручки крана машиниста в положение II",
                "");
    }

    @Override
    public void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(WorkAirDistributorTwoActivity.this);
        builder.setTitle("Работа воздухораспределителя при установке крана машиниста в положение II")
                .setMessage("Зарядите ТМ")
                .setCancelable(true)
                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        measurement.hide();
                        try {
                            AsyncReceiver receiver = new AsyncReceiver(WorkAirDistributorTwoActivity.this);
                            receiver.setMeasurementTimeout(40000);
                            receiver.execute(Requests.WORK_AIR_DISTRIBUTOR_TWO.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
        measurement = builder.create();
        measurement.show();
        setDialogAppearance(measurement);
    }

    @Override
    public void updateMeasurementData(byte command, String data) {
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }

        measurementResult.setText(data);
        measurementResult.append(" кгс/см" + "\u00B2");
        String result = new String(data + " кгс/см" + "\u00B2");
        if (Double.parseDouble(data) < 0.1){
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setAirWorkerSecond(result);
    }
}
