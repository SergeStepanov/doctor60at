package com.niitkd.doctorterminal.ui.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.BatteryTask;
import com.niitkd.doctorterminal.ui.OnDataSentToActivity;

public abstract class MeasurementActivity  extends AppCompatActivity implements View.OnClickListener, OnDataSentToActivity{

    protected TextView measurementTitle;
    protected TextView measurementDescription;
    protected TextView measurementResult;
    protected TextView measurementIsNorm;

    protected AlertDialog failure;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_measurement);

        Button startButton = findViewById(R.id.startButton);

        measurementTitle = findViewById(R.id.measurementName);
        measurementDescription = findViewById(R.id.description);
        measurementResult = findViewById(R.id.measurementResult);
        measurementIsNorm = findViewById(R.id.isNorm);

        startButton.setOnClickListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Проблема с соединением")
                .setMessage("Повторите измерение")
                .setCancelable(true)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        failure = builder.create();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void setDialogAppearance (AlertDialog dialog) {
        TextView title = dialog.findViewById(android.R.id.title);
        if (title != null) {
            title.setTextSize(32);
        }
        TextView firstMeasurementMessage = dialog.findViewById(android.R.id.message);
        if (firstMeasurementMessage != null) {
            firstMeasurementMessage.setTextSize(32);
        }
        Button confirm = dialog.findViewById(android.R.id.button1);
        if (confirm != null) {
            confirm.setTextSize(32);
        }
        Button reject = dialog.findViewById(android.R.id.button2);
        if (reject != null) {
            reject.setTextSize(32);
        }
    }

    public void setMeasurementTextViews(String title, String result, String description, String isNorm) {
        measurementTitle.setText(title);
        measurementResult.setText(result);
        measurementDescription.setText(description);
        measurementIsNorm.setText(isNorm);
    }

    public abstract void updateMeasurementData(byte command, String data);

}
