package com.niitkd.doctorterminal.ui.activities.km394;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.IOException;

public class AirPassageActivity extends MeasurementActivity {

    private AlertDialog rejectMeasurement;
    private AlertDialog measurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setMeasurementTextViews(
                getResources().getString(R.string.kmAirPassage),
                getResources().getString(R.string.density_default),
                "Время, за которое давление в главных резервуарах снизится с 6,0 до 5,0 кгс/см\u00B2 " +
                        " при установке ручки крана машиниста в положение «II», открытом концевом кране" +
                        " и начальном давлении в главных резервуарах 8 кгс/см\u00B2",
                ""
        );
    }

    @Override
    public void onClick(View view) {
        try {
            AsyncReceiver receiver = new AsyncReceiver(AirPassageActivity.this);
            receiver.setMeasurementTimeout(10000);
            receiver.execute(Requests.SOURCE_DENSITY_STATE.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateMeasurementData(byte command, String data) {

        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }
        if (command == Requests.SOURCE_DENSITY_STATE.getCommand()) {
            if (data.equals("cancel")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AirPassageActivity.this);
                builder.setTitle(getResources().getString(R.string.kmAirPassage))
                        .setMessage("Проверка невозможна. Давление в главных резервуарах меньше 8 кгс/см\u00B2")
                        .setCancelable(true)
                        .setNegativeButton("ОК",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                rejectMeasurement = builder.create();
                rejectMeasurement.show();
                setDialogAppearance(rejectMeasurement);
                return;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(AirPassageActivity.this);
            builder.setTitle(getResources().getString(R.string.kmAirPassage))
                    .setMessage("Установите ручку крана машиниста в положение II и откройте концевой кран")
                    .setCancelable(true)
                    .setNegativeButton("Отмена",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                    .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                measurement.hide();
                                AsyncReceiver receiver = new AsyncReceiver(AirPassageActivity.this);
                                receiver.setMeasurementTimeout(65000);
                                receiver.execute(Requests.AIR_PASSAGE.getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            measurement = builder.create();
            measurement.show();
            setDialogAppearance(measurement);
            return;
        }
        if (command == Requests.AIR_PASSAGE.getCommand() && data.equals("success")) {
            return;
        }
        if (command == Requests.AIR_PASSAGE.getCommand()) {

            TextView textView = findViewById(R.id.measurementResult);
            textView.setText(data);
            textView.append(" c");
        }

        String result = new String(data + " c");

        if (Double.parseDouble(data) <= 20) {
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setKmAirPassage(result);

    }
}
