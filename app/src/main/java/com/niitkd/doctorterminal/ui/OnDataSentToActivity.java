package com.niitkd.doctorterminal.ui;

public interface OnDataSentToActivity {
    void updateMeasurementData(byte command, String data);
}
