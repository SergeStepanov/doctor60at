package com.niitkd.doctorterminal.ui.activities.other;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.IOException;

public class CompressorEfficiencyActivity extends MeasurementActivity {

    private AlertDialog rejectMeasurement;
    private AlertDialog measurementFirst;
    private AlertDialog measurementSecond;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMeasurementTextViews(
                getResources().getString(R.string.compressorEfficiency),
                getResources().getString(R.string.density_default),
                "Время, за которое давление в главных резервуарах повысится с 7 до 8 кгс/см\u00B2\n(норма: не более 30 с.)",
                ""
        );
    }

    @Override
    public void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CompressorEfficiencyActivity.this);
        builder.setTitle(getResources().getString(R.string.compressorEfficiency))
                .setMessage("Зарядите главный резервуар. Выключите компрессор")
                .setCancelable(true)
                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            measurementFirst.hide();
                            AsyncReceiver receiver = new AsyncReceiver(CompressorEfficiencyActivity.this);
                            receiver.setMeasurementTimeout(90000);
                            receiver.execute(Requests.COMPRESSOR_CHARGE.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
        measurementFirst = builder.create();
        measurementFirst.show();
        setDialogAppearance(measurementFirst);
    }

    @Override
    public void updateMeasurementData(byte command, String data) {
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }
        if (command == Requests.COMPRESSOR_CHARGE.getCommand()) {
            if (data.equals("cancel")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CompressorEfficiencyActivity.this);
                builder.setTitle(getResources().getString(R.string.compressorEfficiency))
                        .setMessage("Проверка невозможна")
                        .setCancelable(true)
                        .setNegativeButton("ОК",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                rejectMeasurement = builder.create();
                rejectMeasurement.show();
                setDialogAppearance(rejectMeasurement);
                return;
            }
        }
        if (command == Requests.COMPRESSOR_CHARGE.getCommand() && data.equals("success")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(CompressorEfficiencyActivity.this);
            builder.setTitle(getResources().getString(R.string.compressorEfficiency))
                    .setMessage("Включите компрессор")
                    .setCancelable(true)
                    .setNegativeButton("Отмена",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                    .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                measurementSecond.hide();
                                AsyncReceiver receiver = new AsyncReceiver(CompressorEfficiencyActivity.this);
                                receiver.setMeasurementTimeout(65000);
                                receiver.execute(Requests.COMPRESSOR_EFFICIENCY.getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            measurementSecond = builder.create();
            measurementSecond.show();
            setDialogAppearance(measurementSecond);
            return;
        }
        if (command == Requests.COMPRESSOR_EFFICIENCY.getCommand()) {


            TextView textView = findViewById(R.id.measurementResult);
            textView.setText(data);
            textView.append(" c");
        }
        String result = new String(data + " c");
        if (Double.parseDouble(data) <= 30) {
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setCompressorProductivity(result);
    }
}
