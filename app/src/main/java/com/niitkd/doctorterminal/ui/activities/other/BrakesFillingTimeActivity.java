package com.niitkd.doctorterminal.ui.activities.other;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.IOException;

public class BrakesFillingTimeActivity extends MeasurementActivity {

    private AlertDialog measurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMeasurementTextViews(
                getResources().getString(R.string.timeFillingBrakeBus),
                getResources().getString(R.string.density_default),
                "Время, за которое давление в тормозной магистрали увеличится с 0 до 5,0 кгс/см\u00B2\n(норма: не более 4 с.)",
                "");
    }

    @Override
    public void updateMeasurementData(byte command, String data) {
        if (command == Requests.CHECK_CONNECTION_STATUS.getCommand())
            return;
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }
        if (data.equals("success")) {
            return;
        }


        TextView textView = findViewById(R.id.measurementResult);
        textView.setText(data);
        textView.append(" c");


        String result = new String(data + " c");
        TextView isNorm = findViewById(R.id.isNorm);
        if (Double.valueOf(data) < 4){
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setBreaksFillingTime(result);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.startButton) {
            AlertDialog.Builder builder = new AlertDialog.Builder(BrakesFillingTimeActivity.this);
            builder.setTitle("Измерение времени отпуска")
                    .setMessage("Установите КМ в 6 положение, разрядите УР и ТМ до 0 кгс/см\u00B2")
                    .setCancelable(true)
                    .setNegativeButton("Отмена",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                    .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            measurement.hide();
                            try {
                                AsyncReceiver receiver = new AsyncReceiver(BrakesFillingTimeActivity.this);
                                receiver.setMeasurementTimeout(70000);
                                receiver.execute(Requests.TIME_FILLING_BRAKE.getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            measurement = builder.create();
            measurement.show();
            setDialogAppearance(measurement);
        }
    }
}
