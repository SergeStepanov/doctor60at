package com.niitkd.doctorterminal.ui.activities.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.ui.activities.km394.AirPassageActivity;
import com.niitkd.doctorterminal.ui.activities.km394.BrakesOverstressActivity;
import com.niitkd.doctorterminal.ui.activities.km394.EmergencyBrakingActivity;
import com.niitkd.doctorterminal.ui.activities.km394.OverchargeActivity;
import com.niitkd.doctorterminal.ui.activities.km394.ServiceBrakingActivity;
import com.niitkd.doctorterminal.ui.activities.km394.SourceSensibilityFourthActivity;
import com.niitkd.doctorterminal.ui.activities.km394.TankStorageSensibility;
import com.niitkd.doctorterminal.ui.activities.km394.DensityEqualizationTankActivity;
import com.niitkd.doctorterminal.ui.activities.km394.SourceSensibilitySecondActivity;
import com.niitkd.doctorterminal.ui.activities.km394.SourceSensibilityThirdActivity;
import com.niitkd.doctorterminal.ui.activities.km394.ValveSensibilityActivity;

public class MenuKmActivity extends AppCompatActivity implements View.OnClickListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_km);

       // Button kmAirPassageButton = findViewById(R.id.kmAirPassage);
        Button kmOverstressingPressureBrakeBusButton = findViewById(R.id.kmOverstressingPressureBrakeBus);
        //Button kmSensivityPistonYRButton = findViewById(R.id.kmSensivityPistonYR);
        Button kmTimeEliminationOverchargeButton = findViewById(R.id.kmTimeEliminationOvercharge);
        Button kmTimeEmergencyBrakingButton = findViewById(R.id.kmTimeEmergencyBraking);
        Button kmTimeServiceBrakingButton = findViewById(R.id.kmTimeServiceBraking);
        Button densityYRButton = findViewById(R.id.densityYR);
        Button sensivityPowerThreeButton = findViewById(R.id.sensivityPowerThree);
        Button sensivityPowerTwoButton = findViewById(R.id.sensivityPowerTwo);
        Button sensivityPowerFourButton = findViewById(R.id.sensivityPowerFour);

        //kmAirPassageButton.setOnClickListener(this);
        kmOverstressingPressureBrakeBusButton.setOnClickListener(this);
        //kmSensivityPistonYRButton.setOnClickListener(this);
        kmTimeEliminationOverchargeButton.setOnClickListener(this);
        kmTimeEmergencyBrakingButton.setOnClickListener(this);
        kmTimeServiceBrakingButton.setOnClickListener(this);
        densityYRButton.setOnClickListener(this);
        sensivityPowerThreeButton.setOnClickListener(this);
        sensivityPowerTwoButton.setOnClickListener(this);
        sensivityPowerFourButton.setOnClickListener(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void onClick(View view) {
        switch(view.getId()) {
            //case R.id.kmAirPassage:
             //   startActivity(new Intent(this, AirPassageActivity.class));
              //  break;
            case R.id.kmOverstressingPressureBrakeBus:
                startActivity(new Intent(this, BrakesOverstressActivity.class));
                break;
            //case R.id.kmSensivityPistonYR:
              //  startActivity(new Intent(this, TankStorageSensibility.class));
               // break;
            case R.id.kmTimeEliminationOvercharge:
                startActivity(new Intent(this, OverchargeActivity.class));
                break;
            case R.id.kmTimeEmergencyBraking:
                startActivity(new Intent(this, EmergencyBrakingActivity.class));
                break;
            case R.id.kmTimeServiceBraking:
                startActivity(new Intent(this, ServiceBrakingActivity.class));
                break;
            case R.id.densityYR:
                startActivity(new Intent(this, DensityEqualizationTankActivity.class));
                break;
            case R.id.sensivityPowerTwo:
                startActivity(new Intent(this, SourceSensibilitySecondActivity.class));
                break;
            case R.id.sensivityPowerThree:
                startActivity(new Intent(this, SourceSensibilityThirdActivity.class));
                break;
            case R.id.sensivityPowerFour:
                startActivity(new Intent(this, SourceSensibilityFourthActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
