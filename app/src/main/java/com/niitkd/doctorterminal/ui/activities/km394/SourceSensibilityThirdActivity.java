package com.niitkd.doctorterminal.ui.activities.km394;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.IOException;

public class SourceSensibilityThirdActivity extends MeasurementActivity {

    private AlertDialog measurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMeasurementTextViews(
                getResources().getString(R.string.sensivityPowerThree),
                getResources().getString(R.string.sensibility_default),
                "Снижение давления в тормозной магистрали за 30 с. " +
                        "при установке ручки крана машиниста в положение «III»\n(норма: давление не должно восстанавливаться)",
                "");
    }

    @Override
    public void updateMeasurementData(byte command, String data) {
        if (command == Requests.CHECK_CONNECTION_STATUS.getCommand())
            return;
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }

        if (data.equals("success")) {
            return;
        }

        measurementResult.setText(data);
        measurementResult.append(" кгс/см" + "\u00B2");

        String result = new String(data + " кгс/см" + "\u00B2");
        if (Double.parseDouble(data) < 0.05){
            measurementIsNorm.setText("Давление не восстанавливалось. Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setSourceSensibilityThird(result);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.startButton) {
            AlertDialog.Builder builder = new AlertDialog.Builder(SourceSensibilityThirdActivity.this);
            builder.setTitle(getResources().getString(R.string.sensivityPowerThree))
                    .setMessage("Зарядите УР и ТМ. Установите ручку крана машиниста в положение «III»")
                    .setCancelable(true)
                    .setNegativeButton("Отмена",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                    .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            measurement.hide();
                            try {
                                AsyncReceiver receiver = new AsyncReceiver(SourceSensibilityThirdActivity.this);
                                receiver.setMeasurementTimeout(97000);
                                receiver.execute(Requests.SENSIBILITY_POWER_THREE.getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            measurement = builder.create();
            measurement.show();
            setDialogAppearance(measurement);
        }
    }
}
