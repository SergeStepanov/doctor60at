package com.niitkd.doctorterminal.ui.activities.other;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.IOException;

public class BUActivity extends MeasurementActivity {

    private AlertDialog measurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMeasurementTextViews(
                "Работа блокировочного устройства",
                getResources().getString(R.string.density_default),
                "Время, за которое давление в главных резервуарах снизится с 6 до 5 кгс/см\u00B2" +
                        " при установке ручки крана машиниста в положение I",
                "");
    }

    @Override
    public void updateMeasurementData(byte command, String data){
        if (command == Requests.CHECK_CONNECTION_STATUS.getCommand())
            return;
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }

        if (data.equals("success")) {
            return;
        }

        TextView textView = findViewById(R.id.measurementResult);
        textView.setText(data);
        textView.append(" кгс/см" + "\u00B2");


        String result = new String(data + " кгс/см" + "\u00B2");
        if(Double.parseDouble(data) < 0.5){
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setBuWorker(result);
    }

    @Override
    public void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(BUActivity.this);
        builder.setTitle("Работа БУ" )
                .setMessage("Наполните главные резервуары. Выключите компрессор. Переведите ручку КМ в I положение.")
                .setCancelable(true)
                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        measurement.hide();
                        try {
                            AsyncReceiver receiver = new AsyncReceiver(BUActivity.this);
                            receiver.setMeasurementTimeout(40000);
                            receiver.execute(Requests.WORK_BY_FIRST.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
        measurement = builder.create();
        measurement.show();
        setDialogAppearance(measurement);
    }
}
