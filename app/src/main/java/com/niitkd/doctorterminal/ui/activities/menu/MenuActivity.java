package com.niitkd.doctorterminal.ui.activities.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.niitkd.doctorterminal.R;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        //Button kvtGroupButton = findViewById(R.id.kvtGroupButton);
        Button kmGroupButton = findViewById(R.id.kmGroupButton);
        Button otherGroupButton = findViewById(R.id.otherGroupButton);

       // kvtGroupButton.setOnClickListener(this);
        kmGroupButton.setOnClickListener(this);
        otherGroupButton.setOnClickListener(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.kmGroupButton:
                startActivity(new Intent(this, MenuKmActivity.class));
                break;
           /* case R.id.kvtGroupButton:
                startActivity(new Intent(this, MenuKvtActivity.class));
                break; */
            case R.id.otherGroupButton:
                startActivity(new Intent(this, MenuOtherActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
