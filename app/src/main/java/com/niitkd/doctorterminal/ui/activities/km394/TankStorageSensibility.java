package com.niitkd.doctorterminal.ui.activities.km394;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.Parser;
import com.niitkd.doctorterminal.communication.Response;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.OnDataSentToActivity;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;

public class TankStorageSensibility extends MeasurementActivity{

    private AlertDialog measurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setMeasurementTextViews(
                getResources().getString(R.string.kmSensivityPistonYR),
                getResources().getString(R.string.sensibility_default),
                "Снижение давления в уравнительном резервуаре, при котором произойдет" +
                        " такое же снижение давления в тормозной магистрали\n(норма: не более 0.2 кгс/см\u00B2)",
                "");
    }

    @Override
    public void updateMeasurementData(byte command, String data) {
        if (command == Requests.CHECK_CONNECTION_STATUS.getCommand())
            return;
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }

        if (data.equals("success")) {
            return;
        }

        measurementResult.setText(data);
        measurementResult.append(" кгс/см" + "\u00B2");

        String result = new String(data + " кгс/см" + "\u00B2");
        if(Double.parseDouble(data) < 0.2 && Double.parseDouble(data) > 0.15){
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setKmTankStorageSensibility(result);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.startButton) {
            AlertDialog.Builder builder = new AlertDialog.Builder(TankStorageSensibility.this);
            builder.setTitle("Измерение чувствительности УР")
                    .setMessage("Зарядите УР")
                    .setCancelable(true)
                    .setNegativeButton("Отмена",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                    .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            measurement.hide();
                            try {
                                AsyncReceiver receiver = new AsyncReceiver(TankStorageSensibility.this);
                                receiver.setMeasurementTimeout(30000);
                                receiver.execute(Requests.SENSIBILITY_STORAGE_TANK.getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            measurement = builder.create();
            measurement.show();
            setDialogAppearance(measurement);
        }
    }
}
