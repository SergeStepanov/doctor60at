package com.niitkd.doctorterminal.ui.activities.menu;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.BatteryTask;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.niitkd.doctorterminal.services.ReportService.clearReport;
import static com.niitkd.doctorterminal.utils.BatteryHelper.setImages;

/**
 * Created by KruzeII on 26.12.2017.
 */

public class MainMenuActivity extends AppCompatActivity implements View.OnClickListener{

    private BatteryTask batteryTask;
    private List<ImageView> imageViews = new ArrayList<ImageView>() {};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        Button runDiagnosticButton = findViewById(R.id.runDiagnosticsButton);
        Button changeLocoButton = findViewById(R.id.changeLocoButton);
        Button reportButton = findViewById(R.id.reportButton);

        imageViews.add((ImageView)findViewById (R.id.imgBatteryCellEmpty));
        imageViews.add((ImageView)findViewById (R.id.imgBatteryCellBeforeHalf));
        imageViews.add((ImageView)findViewById (R.id.imgBatteryCellAfterHalf));
        imageViews.add((ImageView)findViewById (R.id.imgBatteryCellFull));


        reportButton.setOnClickListener(this);
        runDiagnosticButton.setOnClickListener(this);
        changeLocoButton.setOnClickListener(this);

        setImages(imageViews);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onStart() {
        super.onStart();
        batteryTask = new BatteryTask(this);
        batteryTask.execute(Requests.BATTERY.getBytes());
    }

    public void onClick(View view) {
        batteryTask.cancel(true);
        switch(view.getId()) {
            case R.id.runDiagnosticsButton:
                startActivity(new Intent(this, MenuActivity.class));
                break;
            case R.id.changeLocoButton:
                clearReport();
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.reportButton:
                String timeStamp = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy").format(Calendar.getInstance().getTime());
                ReportService.setTime(timeStamp);
                createReport();
                break;
            default:
                break;
        }
    }

    private void createReport() {
        ReportService.makeReport();
        AlertDialog.Builder builder = new AlertDialog.Builder(MainMenuActivity.this)
                .setTitle("Отчет")
                .setMessage("Отчет успешно создан")
                .setCancelable(true)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        builder.create().show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        batteryTask.cancel(true);
    }
}
