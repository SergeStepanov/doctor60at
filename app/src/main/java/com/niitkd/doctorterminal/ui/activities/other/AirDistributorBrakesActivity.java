package com.niitkd.doctorterminal.ui.activities.other;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.Parser;
import com.niitkd.doctorterminal.communication.Response;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.OnDataSentToActivity;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;

public class AirDistributorBrakesActivity extends MeasurementActivity {

    private AlertDialog measurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMeasurementTextViews(
                getResources().getString(R.string.valveDensity),
                getResources().getString(R.string.sensibility_default),
                "Давление в тормозных цилиндрах при снижении давления в тормозной магистрали на (0,5 - 0,6) кгс/см\u00B2 " +
                        "по истечении 5 минут",
                "");
    }

    @Override
    public void updateMeasurementData(byte command, String data){
        if (command == Requests.CHECK_CONNECTION_STATUS.getCommand())
            return;
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }

        if (data.equals("success")) {
            return;
        }

        TextView textView = findViewById(R.id.measurementResult);
        textView.setText(data);
        textView.append(" кгс/см" + "\u00B2");


        String result = new String(data + " кгс/см" + "\u00B2");
        if(Double.parseDouble(data) < 0.5){
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setAirWorkerBrakes(result);
    }

    @Override
    public void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AirDistributorBrakesActivity.this);
        builder.setTitle("Работа воздухораспределителя после ступени торможения" )
                .setMessage("Зарядите тормозную магистраль")
                .setCancelable(true)
                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        measurement.hide();
                        try {
                            ValveDensityReceiver receiver = new ValveDensityReceiver(AirDistributorBrakesActivity.this);
                            receiver.setMeasurementTimeout(70000);
                            receiver.execute(Requests.WORK_AIRDISTRIBUTOR.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
        measurement = builder.create();
        measurement.show();
        setDialogAppearance(measurement);
    }

    private static class ValveDensityReceiver extends AsyncReceiver {
        public ValveDensityReceiver(OnDataSentToActivity activity) throws IOException {
            super(activity);
        }

        @Override
        protected Response doInBackground(byte[]... requests) {
            Response measurementResult = new Response(requests[0]);
            Response measurementStarted;
            Socket socket = new Socket();

            try {
                socket = new Socket(InetAddress.getByName("192.168.1.55"), 88);
                DataOutputStream out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                DataInputStream is = new DataInputStream(socket.getInputStream());

                out.write(requests[0]);
                out.flush();

                Parser parser = new Parser();

                byte[] inputBuffer = new byte[8];

                //
                //После посылки реквеста на измерение сервер должен ответить кодом о готовности.
                //Здесь ловим этот код.
                //
                waitForResponse(is, inputBuffer, getCommunicationTimeout());
                measurementStarted = parser.parseResponse(inputBuffer);
                Arrays.fill(inputBuffer, (byte) 0);
                socket.close();
                if (measurementStarted.getData().equals("success") &&
                        measurementStarted.getCommand() != Requests.CHECK_CONNECTION_STATUS.getCommand()) {
                    Thread.sleep(270000);
                    socket = new Socket(InetAddress.getByName("192.168.1.55"), 88);
                    out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                    is = new DataInputStream(socket.getInputStream());

                    long waitingTime = 0;

                    while (waitingTime < getMeasurementTimeout()) {
                        waitForResponse(is, inputBuffer, getMeasurementTimeout());
                        if (inputBuffer[0] != 0 && inputBuffer[1] != 0)
                            measurementResult = parser.parseResponse(inputBuffer);
                        Arrays.fill(inputBuffer, (byte) 0);
                        waitingTime = getMeasurementTimeout();
                        if(measurementResult.getData().equals("waiting")) {
                            waitingTime = 0;
                            measurementResult.setData("failure");
                        }
                    }
                }

                if (measurementResult.getData().equals("failure")) {
                    out.write(Requests.RESTART_WIFI_MODULE.getBytes());
                    out.flush();
                    publishProgress();
                    Thread.sleep(25000);
                }
            } catch (IOException e) {
                setException(e);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return measurementResult;
        }
    }
}
