package com.niitkd.doctorterminal.ui.activities.km394;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.IOException;

public class ValveSensibilityActivity extends MeasurementActivity {

    private AlertDialog measurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMeasurementTextViews(
                getResources().getString(R.string.valveSensibility),
                getResources().getString(R.string.sensibility_default),
                "Изменение давления в тормозной магистрали в течение 1 минуты",
                "");
        createDialogs();
    }

    @Override
    public void onClick(View view) {
        measurement.show();
        setDialogAppearance(measurement);
    }

    @Override
    public void updateMeasurementData(byte command, String data) {
        if (command == Requests.CHECK_CONNECTION_STATUS.getCommand())
            return;
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }

        if (data.equals("success")) {
            return;
        }

        TextView textView = findViewById(R.id.measurementResult);
        textView.setText(data);
        textView.append(" кгс/см" + "\u00B2");

        TextView normTextView = findViewById(R.id.normTextView);


        String result = new String(data + " кгс/см" + "\u00B2");
        if(Double.parseDouble(data) < 0.1){
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setValveSensibility(result);
    }

    protected void createDialogs() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ValveSensibilityActivity.this);
        builder.setTitle("Проверка чувствительности золотникового питательного клапана")
                .setMessage("Установите ручку крана машиниста в поездное положение. Зарядите УР и ТМ")
                .setCancelable(true)
                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        measurement.hide();
                        try {
                            AsyncReceiver receiver = new AsyncReceiver(ValveSensibilityActivity.this);
                            receiver.setMeasurementTimeout(75000);
                            receiver.execute(Requests.VALVE_SENSIBILITY.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
        measurement = builder.create();
    }

}
