package com.niitkd.doctorterminal.ui.activities.kvt;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.communication.AsyncReceiver;
import com.niitkd.doctorterminal.communication.codes.Requests;
import com.niitkd.doctorterminal.services.ReportService;
import com.niitkd.doctorterminal.ui.activities.MeasurementActivity;

import java.io.IOException;

public class KvtBrakesCylindersDensityActivity extends MeasurementActivity {

    AlertDialog measurement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMeasurementTextViews(
                getResources().getString(R.string.kvtBrakeCylinderDensity),
                getResources().getString(R.string.sensibility_default),
                "Снижение давления в тормозных цилиндрах с (2,7 - 3,0) кгс/см² за 60 с",
                "");
    }

    @Override
    public void updateMeasurementData(byte command, String data) {
        if (command == Requests.CHECK_CONNECTION_STATUS.getCommand())
            return;
        if (data.equals("failure")) {
            failure.show();
            setDialogAppearance(failure);
            return;
        }

        if (data.equals("success")) {
            return;
        }

        measurementResult.setText(data);
        measurementResult.append(" кгс/см" + "\u00B2");


        String result = new String(data + " кгс/см" + "\u00B2");
        if(Double.parseDouble(data) > 0.2){
            measurementIsNorm.setText("Значение входит в нормированный диапазон");
            result = result.concat(". Норма");
        } else {
            measurementIsNorm.setText("Значение не входит в нормированный диапазон");
            result = result.concat(". Вне нормы");
        }
        ReportService.setKvtBreaksCylindersDensity(result);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.startButton) {
            AlertDialog.Builder builder = new AlertDialog.Builder(KvtBrakesCylindersDensityActivity.this);
            builder.setTitle("Измерение плотности ТЦ")
                    .setMessage("Переведите ручку крана вспомогательного тормоза в V положение")
                    .setCancelable(true)
                    .setNegativeButton("Отмена",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                    .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            measurement.hide();
                            try {
                                AsyncReceiver receiver = new AsyncReceiver(KvtBrakesCylindersDensityActivity.this);
                                receiver.setMeasurementTimeout(70000);
                                receiver.execute(Requests.DENSITY_BRAKE_CYLINDER.getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            measurement = builder.create();
            measurement.show();
            setDialogAppearance(measurement);
        }
    }
}
