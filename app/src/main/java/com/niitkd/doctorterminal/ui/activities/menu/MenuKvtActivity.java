package com.niitkd.doctorterminal.ui.activities.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.niitkd.doctorterminal.R;
import com.niitkd.doctorterminal.ui.activities.kvt.KvtBrakesCylindersDensityActivity;
import com.niitkd.doctorterminal.ui.activities.kvt.KvtBrakesCylindersPressureActivity;
import com.niitkd.doctorterminal.ui.activities.kvt.KvtFillingTimeActivity;
import com.niitkd.doctorterminal.ui.activities.kvt.KvtReleaseTimeActivity;

public class MenuKvtActivity extends AppCompatActivity implements View.OnClickListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_kvt);

        Button kvtBrakesCylindersDensityButton = findViewById(R.id.kvtBrakesCylindersDensity);
        Button kvtBrakesCylindersPressureButton = findViewById(R.id.kvtBrakesCylindersPressure);
        Button kvtFillingTimeButton = findViewById(R.id.kvtFillingTime);
        Button kvtReleaseTimeButton = findViewById(R.id.kvtReleaseTime);

        kvtBrakesCylindersDensityButton.setOnClickListener(this);
        kvtBrakesCylindersPressureButton.setOnClickListener(this);
        kvtFillingTimeButton.setOnClickListener(this);
        kvtReleaseTimeButton.setOnClickListener(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.kvtBrakesCylindersDensity:
                startActivity(new Intent(this, KvtBrakesCylindersDensityActivity.class));
                break;
            case R.id.kvtBrakesCylindersPressure:
                startActivity(new Intent(this, KvtBrakesCylindersPressureActivity.class));
                break;
            case R.id.kvtFillingTime:
                startActivity(new Intent(this, KvtFillingTimeActivity.class));
                break;
            case R.id.kvtReleaseTime:
                startActivity(new Intent(this, KvtReleaseTimeActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
