package com.niitkd.doctorterminal.utils;
import android.view.View;
import android.widget.ImageView;

import com.niitkd.doctorterminal.R;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by KruzeII on 21.12.2017.
 */

public class BatteryHelper {

    private static Map<String, ImageView> batteryCells = new HashMap<>();

    public static void setImages (List<ImageView> imageViews) {
        batteryCells.put("EMPTY", imageViews.get(0));
        batteryCells.put("BEFORE_HALF", imageViews.get(1));
        batteryCells.put("AFTER_HALF", imageViews.get(2));
        batteryCells.put("FULL", imageViews.get(3));
    }

    public static void changeBatteryView(String data) {
        if(Double.valueOf(data) > 80) {
            batteryCells.get("EMPTY").setVisibility(View.VISIBLE);
            batteryCells.get("BEFORE_HALF").setVisibility(View.VISIBLE);
            batteryCells.get("AFTER_HALF").setVisibility(View.VISIBLE);
            batteryCells.get("FULL").setVisibility(View.VISIBLE);
        } else  if (Double.valueOf(data) > 50 && Double.valueOf(data) <= 80) {
            batteryCells.get("EMPTY").setVisibility(View.VISIBLE);
            batteryCells.get("BEFORE_HALF").setVisibility(View.VISIBLE);
            batteryCells.get("AFTER_HALF").setVisibility(View.VISIBLE);
            batteryCells.get("FULL").setVisibility(View.INVISIBLE);
        } else if (Double.valueOf(data) > 25 && Double.valueOf(data) <= 50) {
            batteryCells.get("EMPTY").setVisibility(View.VISIBLE);
            batteryCells.get("BEFORE_HALF").setVisibility(View.VISIBLE);
            batteryCells.get("AFTER_HALF").setVisibility(View.INVISIBLE);
            batteryCells.get("FULL").setVisibility(View.INVISIBLE);
        } else if (Double.valueOf(data) > 7 && Double.valueOf(data) <= 25) {
            batteryCells.get("EMPTY").setVisibility(View.VISIBLE);
            batteryCells.get("BEFORE_HALF").setVisibility(View.INVISIBLE);
            batteryCells.get("AFTER_HALF").setVisibility(View.INVISIBLE);
            batteryCells.get("FULL").setVisibility(View.INVISIBLE);
        } else {
            batteryCells.get("EMPTY").setVisibility(View.INVISIBLE);
            batteryCells.get("BEFORE_HALF").setVisibility(View.INVISIBLE);
            batteryCells.get("AFTER_HALF").setVisibility(View.INVISIBLE);
            batteryCells.get("FULL").setVisibility(View.INVISIBLE);
        }
    }
}
